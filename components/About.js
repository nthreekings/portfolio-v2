import React, { useEffect } from 'react';
import { Container, Row,  Col } from 'react-bootstrap';
import Aos from 'aos';
import 'aos/dist/aos.css';

export default function About() {

	useEffect(() => {
	    Aos.init();
	}, [])

	return (

		<Container>
			<div data-aos="fade-right" data-aos-duration="1000">
				<h1 className="display-1 my-5">ABOUT ME</h1>
			</ div>
		
            <Row>
                <Col className="mx-auto" sm="7" md="7" lg="9">
                    <div data-aos="fade-up" data-aos-duration="1000">
                        <h1 className="display-6">I'm a frontend developer and a UI/UX designer with a passion for design and web development.</h1>
                    </div>

                    <div className="mt-4" data-aos="fade-up" data-aos-duration="1000">
                        <h1 className="display-6">I design performance driven, responsive websites that work seamlessly across devices. Personally, I love making clean and simple interfaces.</h1>
                    </div>

                    <div className="mt-4" data-aos="fade-up" data-aos-duration="1000">
                        <h1 className="display-6">On my free time, I love to drink a cup of coffee and play with my dog. I like to watch crime documentaries as well.</h1>
                    </div>

                    <div className="mt-5" data-aos="fade-up" data-aos-duration="1000">
                        <h5>SKILLSET</h5>
                        <Row>
                        	<Col xs="6" md="3">
                        		<ul className="p-0">
                        			<li>HTML</li>
                        			<li>CSS</li>
                        			<li>Javascript</li>
                        			<li>Bootstrap</li>
                        		</ul>
                        	</Col>

                        	<Col xs="6" md="3">
                        		<ul className="p-0">
                        			<li>MongoDB</li>
                        			<li>Express</li>
                        			<li>Node.js</li>
                        			<li>Reactjs/Nextjs</li>
                        		</ul>
                        	</Col>

                        	<Col xs="6" md="3">
                        		<ul className="p-0">
                        			<li>Photoshop</li>
                        			<li>Illustrator</li>
                        			<li>After Effects</li>
                        			<li>Figma</li>
                        		</ul>
                        	</Col>

                        	<Col xs="6" md="3">
                        		<ul className="p-0">
                        			<li>GitLab</li>
                        			<li>Postman</li>
                        			<li>Heroku</li>
                        			<li>Vercel</li>
                        		</ul>
                        	</Col>

                        </Row>
                    </div>
                </Col>        
            </Row>
        </Container>

	)

}