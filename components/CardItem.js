import React from 'react';
import Link from 'next/link';
import { Row, Col } from 'react-bootstrap';

export default function CardItem({path, src, number, title, description}) {

	return(

		<a href={path} target="_blank" className="text-link">
              <div className="gallery-img-wrap">
                    <div className="project-img">
                          <img className="img-fluid gallery-img" src={src}/>
                    </div>

                    <div className="project-text">
                    	<Row>
                    		<Col xs="10" md="10">
                    			<span className="gallery-title">
			                          <h4 className="display-3">{title}</h4>
			                    </span>
			                    <p className="gallery-caption">{description}</p>
                    		</Col>

                    		<Col xs="2" md="2">
                    			<h1 className="display-3">{number}</h1>
                    		</Col>
                    	</Row>	
                    </div>
              </div>
        </a>

	)
}