import React from 'react';
import { Container, Row,  Col } from 'react-bootstrap';
import * as AiIcons from 'react-icons/ai';

export default function Contact() {

	return (

		<Container>
            <div className="my-5" data-aos="fade-right" data-aos-duration="1000">
                  <h1 className="display-1 my-5">GET IN TOUCH</h1>
            </div>
            <Row className="mb-5">
                <Col className="mx-auto" sm="7" md="7" lg="9">
                    <div data-aos="fade-up" data-aos-duration="1000">
                        <h1 className="display-6">If you wanna talk to me about a project collaboration or just want to say hi, feel free to reach out to me on any of my socials</h1>
                    </div>

                    <div className="my-5" data-aos="fade-up" data-aos-duration="1000">
                        <Row>
                            <Col md="4">
                                <ul className="p-0">
                                    <a href="" target="_blank" className="text-link">
                                        <li className="mt-2"><span><AiIcons.AiFillMail /></span> najtatlonghari@gmail.com</li>
                                    </a>
                                    <a href="https://www.linkedin.com/in/nicholetatlonghari/" target="_blank" className="text-link">
                                        <li className="mt-2"><span><AiIcons.AiFillLinkedin /></span> in/nicholetatlonghari</li>
                                    </a>
                                </ul>
                            </Col>

                            <Col md="4">
                                <ul className="p-0">
                                    <a href="https://www.instagram.com/n.threekings/" target="_blank" className="text-link">
                                        <li className="mt-2"><span><AiIcons.AiFillInstagram /></span> @n.threekings</li>
                                    </a>
                                    <a href="https://www.facebook.com/najtatlonghari/" target="_blank" className="text-link">
                                        <li className="mt-2"><span><AiIcons.AiFillFacebook /></span> Nichole Tatlonghari</li>
                                    </a>
                                </ul>
                            </Col>

                            <Col md="4">
                                <ul className="p-0">
                                    <a href="https://dribbble.com/nkabjn" target="_blank" className="text-link">
                                        <li className="mt-2"><span><AiIcons.AiOutlineDribbble /></span> dribbble.com/nkabjn</li>
                                    </a>
                                    <a href="https://gitlab.com/nthreekings" target="_blank" className="text-link">
                                        <li className="mt-2"><span><AiIcons.AiFillGitlab /></span> gitlab.com/nthreekings</li>
                                    </a>
                                </ul>
                            </Col>
                        </Row>
                    </div>
                </Col> 
            </Row>
        </Container>

	)

}