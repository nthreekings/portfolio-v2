import React from 'react';

export default function Footer() {

	return(
		<footer className="mt-5">
			<div className="d-flex justify-content-center">
				<p>Made with &#9829; by Nichole. &#169; 2021</p>
			</div>
		</footer>
	)

}