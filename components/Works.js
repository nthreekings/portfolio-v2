import React, { useEffect } from 'react';
import Link from 'next/link';
import { Container, Row, Col } from 'react-bootstrap';
import Aos from 'aos';
import 'aos/dist/aos.css';
import CardItem from './CardItem';

export default function Works() {

      useEffect(() => {
            Aos.init();
      }, [])

	return(
		<Container className="cards-wrapper">
            <div className="my-5" data-aos="fade-right" data-aos-duration="1000">
                  <h1 className="display-1 my-5">FEATURED WORKS</h1>
            </div>
            <Row >
                <Col className="my-5" lg="6" data-aos="fade-up" data-aos-easing="ease-in-back" data-aos-delay="300" data-offset="0">
                    <CardItem 
                        path='https://nthreekings.gitlab.io/capstone-02-frontend/'
                        src='../../kulas.png'
                        number='01'
                        title='Kulas'
                        description='A course booking application with CRUD functionalities made using Javascript, MongoDB, Express and Nodejs. It allows users to enroll in courses and has admin asset management.'
                    />
                </Col>

                <Col className="my-5" lg="6" data-aos="fade-up" data-aos-easing="ease-in-back" data-aos-delay="800" data-offset="0">
                    <CardItem 
                        path='https://frupal-app.vercel.app/'
                        src='../../frupal.png'
                        number='02'
                        title='FruPal'
                        description='A budget tracking app developed using React/Next js, MongoDB, Express, and Node js. It allows user to track personal incomes and expenses.'
                    />
                </Col>

                <Col className="my-5" lg="6" data-aos="fade-up" data-aos-easing="ease-in-back" data-aos-delay="800" data-offset="0">
                    <CardItem 
                        path='https://personal-journal-py.herokuapp.com/'
                        src='../../journal.png'
                        number='03'
                        title='Journal'
                        description='A personal journal website made using Python/Flask and Bootstrap. It allows you to add, edit, and delete entry. Use the following credentials to access it: (username: admin | password: password123)'
                    />
                </Col>
            </Row>    
        </Container>
	)

}