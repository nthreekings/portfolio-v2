import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Container, Row, Col } from 'react-bootstrap';
import Works from '/components/Works';
import About from '/components/About';
import Contact from '/components/Contact';
import Footer from '/components/Footer';
import Scrollchor from 'react-scrollchor';

export default function Home() {

    const [scrollBar, setscrollBar] = useState(false);

    const changeColor = () => {
        if(window.scrollY >= 750) {
            setscrollBar(true)
        } else {
            setscrollBar(false)
        }
    };

    useEffect(() => {
        window.addEventListener('scroll', changeColor);
    }, [])

    

    return (
        <React.Fragment>
            <Head>
                <title>Nichole Tatlonghari</title>
                <link rel="icon" href="/favicon.ico" />
                <meta property="og:title" content="Nichole Tatlonghari"/>
                <meta property="og:description" content="This is Nichole Tatlonghari's portfolio that showcases his development works."/>
                <meta property="og:image" content="https://gitlab.com/nthreekings/portfolio-v2/-/raw/master/public/thumb.png"/>
                <meta property="og:url" content="https://najtatlonghari.vercel.app/"/>
                <meta name="twitter:card" content="summary_large_image"></meta>
            </Head>
            <Navbar className={scrollBar ? 'active' : ''} sticky="top" expand="lg">
                <Navbar.Brand className={scrollBar ? '' : 'nav-link text-white'}>NAJT</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        <Scrollchor to="#home" className={scrollBar ? 'nav-link' : 'nav-link text-white'} animate={{offset: 0, duration: 100}}>Home</Scrollchor>
                        <Scrollchor to="#portfolio" className={scrollBar ? 'nav-link' : 'nav-link text-white'} animate={{offset: 0, duration: 100}}>Portfolio</Scrollchor>
                        <Scrollchor to="#about" className={scrollBar ? 'nav-link' : 'nav-link text-white'} animate={{offset: 0, duration: 100}}>About</Scrollchor>
                        <Scrollchor to="#contact" className={scrollBar ? 'nav-link' : 'nav-link text-white'} animate={{offset: 0, duration: 100}}>Contact</Scrollchor>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>

            <section id="home">

                <Container className="d-flex align-items-center justify-content-center landing">
                    <div className="block">
                        <h1>Hi, my name is Nichole Tatlonghari</h1> 
                        <h1>a front-end developer and an aspiring</h1>
                        <h1>UI/UX designer</h1> 
                    </div>
                </Container>
            </section>

            <section id="portfolio">
                <Works />
            </section>

            <section id="about">
                <About />
            </section>

            <section id="contact">
                <Contact />
            </section>

            <section>
                <Footer />
            </section>
        </React.Fragment>
    )
}
